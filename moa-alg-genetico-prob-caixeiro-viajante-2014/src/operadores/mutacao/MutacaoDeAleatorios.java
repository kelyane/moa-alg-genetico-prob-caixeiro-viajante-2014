package operadores.mutacao;

import base.Aleatorio;
import base.Solucao;

// Representa o operador de mutação de aleatórios, onde 2 vértices são escolhidos
// aleatoriamente e invertidos no percurso da solução.
public class MutacaoDeAleatorios implements OperadorMutacao {

    @Override
    public void inverte(Solucao s) {
        int tamanho = s.tamanho();

        int posicao1 = Aleatorio.geraEntre(0, tamanho);
        int posicao2;

        do {
            posicao2 = Aleatorio.geraEntre(0, tamanho);
        } while (posicao2 == posicao1);

        s.inverte(posicao1, posicao2);
    }
}
