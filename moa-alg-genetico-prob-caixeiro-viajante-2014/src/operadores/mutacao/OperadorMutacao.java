package operadores.mutacao;

import base.Solucao;

// Representa um operador de mutação.
public interface OperadorMutacao {

    // Aplica o operador a uma solução, invertendo algum(ns) vértice(s).
    public void inverte(Solucao s);
}
