package operadores.crossover;

import base.ParDeSolucoes;

// Representa um operador de crossover.
public interface OperadorCrossover {

    // Recebe um par de soluções, chamadas de pais, realiza combinações entre
    // seus vértices (genes), e retorna outro par de soluções, que são o resultado
    // das combinações dos pais.
    public ParDeSolucoes aplica(ParDeSolucoes pais);
}
