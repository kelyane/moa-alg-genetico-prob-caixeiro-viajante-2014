package operadores.crossover;

// Classe usada para alimentar a caixa de seleção (ComboBox) correspondente na
// tela de parâmetros.
public enum TipoOperadorCrossover {

    CROSSOVER_CX("C.X.", new CrossoverCX()),
    CROSSOVER_OX("O.X.", new CrossoverOX());

    private final String nome;
    private final OperadorCrossover oc;

    TipoOperadorCrossover(String nome, OperadorCrossover oc) {
        this.nome = nome;
        this.oc = oc;
    }

    public OperadorCrossover getOperador() {
        return this.oc;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
