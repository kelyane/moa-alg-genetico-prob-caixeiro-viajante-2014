package operadores.selecao;

import base.ParDeSolucoes;
import base.Populacao;

// Representa um operador de seleção.s
public interface OperadorSelecao {

    // Retorna um par de soluções da população, que será usado para o crossover.
    public ParDeSolucoes seleciona(Populacao populacao);
}
