package base;

import java.util.Random;

// Classe usada para gerar números aleatórios.
public class Aleatorio {

    private static final Random random = new Random();

    // Retorna um número inteiro entre minimo (inclusive) e maximo (exclusive),
    // ou seja, um inteiro no intervalo [minimo, maximo).
    public static int geraEntre(int minimo, int maximo) {
        return minimo + random.nextInt(maximo);
    }
}
