package base;

import java.text.DecimalFormat;
import java.text.NumberFormat;

// Representa um vértice (cidade) para o Problema do Caixeiro Viajante.
// Em um algoritmo genético, corresponde a um gene.
public class Vertice {

    private static final NumberFormat nf = new DecimalFormat("000");
    private final int id;// Identificador do vértice
    private final int coordX;// Coordenada X
    private final int coordY;// Coordenada Y

    public Vertice(int id, int coordX, int coordY) {
        this.id = id;
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public int getId() {
        return id;
    }

    // Retorna a distância do vértice this até o vértice destino.
    public double distanciaAte(Vertice destino) {
        return Math.sqrt(
                Math.pow(this.coordX - destino.coordX, 2)
                + Math.pow(this.coordY - destino.coordY, 2));
    }

    @Override
    public String toString() {
        return "#" + nf.format(id);
    }

    // 2 vértices são iguais quando possuem o mesmo id.
    // Pressupõe-se que não haja id's repetidos em uma solução.
    @Override
    public boolean equals(Object o) {
        if (o instanceof Vertice) {
            Vertice v = (Vertice) o;

            return this.id == v.id;
        }

        return false;
    }
}
