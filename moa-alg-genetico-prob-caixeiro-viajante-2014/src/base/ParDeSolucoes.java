package base;

// Representa um par de soluções.
// Usado no operador de crossover, e como retorno do operador de seleção.
public class ParDeSolucoes {

    private final Solucao solucao1;
    private final Solucao solucao2;

    public ParDeSolucoes(Solucao pai1, Solucao pai2) {
        this.solucao1 = pai1;
        this.solucao2 = pai2;
    }

    public Solucao getSolucao1() {
        return solucao1;
    }

    public Solucao getSolucao2() {
        return solucao2;
    }
}
