package base;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

// Classe usada para interpretar um arquivo de vértices e a partir de seu conteúdo
// gerar uma população inicial de possíveis soluções.
public class GeradorDePopulacaoInicial {

    // Retorna uma população, do tamanho especificado, a partir do conteúdo do
    // arquivo.
    public Populacao gera(File arquivo, int tamanhoDaPopulacao) throws Exception {
        List<Vertice> vertices = geraVerticesAPartirDe(arquivo);

        Populacao populacao = new Populacao();

        for (int i = 0; i < tamanhoDaPopulacao; i++) {
            List<Vertice> copia = new ArrayList<>(vertices);
            Collections.shuffle(copia);
            populacao.adiciona(new Solucao(copia));
        }

        return populacao;
    }

    // Retorna a lista de objetos Vertice correspondente ao conteúdo do arquivo.
    private List<Vertice> geraVerticesAPartirDe(File arquivo) throws Exception {
        List<Vertice> vertices = new ArrayList<>();

        try (Scanner sc = new Scanner(new FileInputStream(arquivo))) {
            // Descarta o cabeçalho
            for (int i = 0; i < 6; i++) {
                sc.nextLine();
            }

            while (sc.hasNextInt()) {
                int id = sc.nextInt();
                int coordX = sc.nextInt();
                int coordY = sc.nextInt();

                vertices.add(new Vertice(id, coordX, coordY));
            }
        } catch (Exception ex) {
            throw new Exception("Houve um erro na interpretação do arquivo.");
        }

        return vertices;
    }
}
