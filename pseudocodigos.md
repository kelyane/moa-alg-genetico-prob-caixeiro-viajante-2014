# Operadores genéticos

## Seleção (por torneio)

Para obter os dois cromossomos reprodutores o algoritmo é chamado duas vezes, com os mesmos argumentos.

```
Torneio(Populacao, K):
	Selecione K elementos de Populacao, aleatoriamente.
	Retorne o elemento com maior aptidão dentre os k selecionados.
```


## Cruzamento

### CX

```
CrossoverCX(Doador1, Doador2):
	Crie as listas vazias Filho1 e Filho2 do mesmo tamanho que Doador1 e Doador2.
	
	V <- Doador1[1].
	Repita:
		i <- Índice de V em Doador1.
		Filho1[i] <- V.
		V <- Doador2[i].
	Até que V = Doador1[1].
	
	Analogamente para Filho2, invertendo os doadores de Filho1.

	Para cada i, tal que Filho1[i] esteja vazio:
		Filho1[i] <- Doador2[i].
		
	Para cada i, tal que Filho2[i] esteja vazio:
		Filho2[i] <- Doador1[i].

	Retorne Filho1 e Filho2.
```


### OX

```
CrossoverOX(Doador1, Doador2):
	Crie as listas vazias Filho1 e Filho2 do mesmo tamanho que Doador1 e Doador2.
	Selecione aleatoriamente dois pontos de corte.
	
	Para cada i entre os pontos de corte:
		Filho1[i] <- Doador2[i].
		Filho2[i] <- Doador1[i].
	
	Crie duas filas vazias, F1 e F2.
	Enfileire em F1 os elementos de Doador1, na ordem em que estão, mas começando do 2º ponto de corte até o final, e então do início até o 2º ponto de corte, sempre ignorando os elementos que já existem em Filho2.
	Analogamente para F2, obtendo os elementos de Doador2 que ainda não existem em Filho1.
	
	Para cada i entre o 2º ponto de corte e o final, e depois entre o início e o 1º ponto de corte:
		Retire o primeiro elemento de F2 e insira-o em Filho1[i].
	
	Analogamente para F1, inserindo os elementos em Filho2[i].
	
	Retorne Filho1 e Filho2.
```


## Mutação

```
Mutacao(Filho, ChancePercentual):
	Sorteie um número N.
	Se N está entre 0 e ChancePercentual:
		Selecione aleatoriamente dois índices de Filho.
		Inverta a posição dos genes dos índices selecionados.
	// Senão: mantém Filho como está.
	Retorne Filho.
```